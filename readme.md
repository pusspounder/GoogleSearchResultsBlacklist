Blacklist for...

```
Personal Blocklist (by Google)
https://chrome.google.com/webstore/detail/personal-blocklist-by-goo/nolijncfnkgaikbjbdaogikpmpbdcdef
```

---

Note: As of December 2016 you still can not block TLDs (Top Level Domains) in Google search results.

Broken:

```
Google Hit Hider by Domain (Search Filter / Block Sites)
Can not block TLDs by design (source:author).
https://greasyfork.org/en/scripts/1682-google-hit-hider-by-domain-search-filter-block-sites
```

```
Google Block Words
Simply appends "-term" to search query.
https://greasyfork.org/en/scripts/15521-google-block-words
```

```
Google Domain Blocker / Google_Domain_Blocker.user.js
Looks promising. RegEx. Last update in 2013 :(
https://github.com/pjobson/pjUserscripts
```